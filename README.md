# m4-bloggery

m4-bloggery is a simple static site generator that depends only on m4,
Make and a Markdown parser.  It is based on m4-bakery[1] by Datagrok.
m4-bloggery adds some extra features for a blog-oriented site, namely
a blog/news index and an Atom XML feed. 

# Requirements

* Make (probably only the GNU variant; I haven't checked if it depends
  on any GNU extensions)
* M4 (any version will probably work)
* Any command-line program to convert Markdown into HTML (optional)

# Usage

## Creating pages

Some example files are provided in the tree.  It is easiest to work
with Markdown files, so I'll only talk about that here.  To create a
new page, simply create a file under the `src/` directory with the
`.md.m4` extension. Inside, you can define several macros:

* TITLE: the title of the page
* DATE: the date the page was created
* UPDATE: the date that a page was updated
* TAGS: a space-separated list of tags for the page (note: I have not
        implemented a proper tagging system)
* BODY: the main text of the page

You define a macro by putting some text in between a nested pair of
parentheses and braces, with the text surrounded by quotes.  So, to
define the title of the page, you would add:

    TITLE({"This is my site"})

The exception is the BODY macro, which requires an extra nested pair
of braces and quotes:

    BODY({"{"
        This is the main text of this page, which is boring.
    "}"})

Why do you have to do that?  Eh...the vagaries of quoting in m4. 

## Creating blog/news posts

There is a shortcut to creating a new post: the `new-post` Make
target.  You must supply a value to the TITLE variable on the
command-line when you use this target:

    $ make new-post TITLE="How amazingly simple"

This will create a new post under the `src/news` sub-directory.  The
file name will include the current date and the hyphen-separated
title.  If you want your blog to be under a different directory (say,
`src/blog`), simply modify the `BLOG` variable in the Makefile.

## Generating the site

Once you have created all of your pages, simply make the `all` target:

    $ make all

This will first run the pages through Markdown to generate the HTML
and then it runs them through m4 to construct full pages out of the
layouts.  This will also automatically generate a news/blog index from
all the posts under `news`, sorted by date.  Finally, it will
automatically generate an Atom XML feed.

The generated site will be moved into a directory called `dst`.
Whatever directory structure you have under `src` will be simply
mirrored to `dst`.  In the end, the contents of `dst` are to be
served.

## Deployment

You can automatically deploy the website to your server via rsync
using the `deploy` make target:

    $ make deploy

You should modify the file `auth.mk` to include your username
(user@server) and the document root where things will be uploaded:

    SSH_USER = me@myserver.com
    DOC_ROOT = ~/public_html

# Customization

You can customize this to your heart's desire.  I hate web design so
the default design is rather minimalist.  Modify the files in the
`templates` directory to change the site structure (particularly
`main.html.m4`).  Notice all of those `m4_divert()` calls?  m4 uses
those to determine where and how to merge the various templates.  You
shouldn't need to change those.

You can modify the css under the `src/css` sub-directory.

The behavior of the Makefile is extremely hackable, so dig in!

# Example Sites

(if you make a site using m4-bloggery, I'd love to hear about it!)

* http://brandon.invergo.net (uses the default design)

# Contact

Brandon Invergo <brandon [at] invergo [dot] net>

# Footnotes

[1]  https://github.com/datagrok/m4-bakery

